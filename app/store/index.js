import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
export const strict = false

export const state = () => ({
  page: 1,
  isLoaded: false,
})
 
export const getters = {
  page: state => state.page,
  isLoaded: state => state.isLoaded
}

export const mutations = {
  savePage(state, { page }){
    state.page = page
  },
  setLoaded(state, { loaded }){
    state.isLoaded = loaded
  },
}

export const actions = {
  nuxtClientInit ({ commit, state, dispatch }, { req }) {  
  },
  SET_PAGE_NUM({commit}, { page }){
    commit('savePage', { page: page })
  },
  LOCK_UI({commit}){
    commit('setLoaded', { loaded: false })
  },
  UNLOCK_UI({commit}){
    commit('setLoaded', { loaded: true })
  },
}

