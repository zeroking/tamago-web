module.exports = {
  srcDir: 'app',
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: 'AI×エンタメ | 株式会社Tamago',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '夢を形に' },
      { name: 'google-site-verification', content:'n9tGpR4PoDjpkWw_uwE01xsCsWO_bRRnSsinv3Q45cY'}
    ],
    link: [
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.4.1/css/all.css' },
      { rel: 'stylesheet', href: 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
  },
  modules: [
    ['nuxt-sass-resources-loader', '~/assets/css/main.scss']
  ],
  css: [
    { src: '~/assets/css/base.css', lang: 'css' },
    { src: 'bulma/bulma.sass', lang: 'sass' },
    '@fortawesome/fontawesome-free-webfonts',
    '@fortawesome/fontawesome-free-webfonts/css/fa-brands.css',
    '@fortawesome/fontawesome-free-webfonts/css/fa-regular.css',
    '@fortawesome/fontawesome-free-webfonts/css/fa-solid.css',
  ],  
  loading: { color: '#00d1b2' },    
  /*
  ** Build configuration
  */
  plugins: [
    { src: '~/plugins/nuxt-client-init.js', ssr: false },
    { src: '~/plugins/vuelidate' }
  ], 
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}

